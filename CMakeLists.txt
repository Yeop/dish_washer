
## BEGIN_TUTORIAL
## This CMakeLists.txt file for rviz_plugin_tutorials builds both the TeleopPanel tutorial and the ImuDisplay tutorial.
##
## First start with some standard catkin stuff.
cmake_minimum_required(VERSION 2.8.3)
project(dish_washer)
find_package(catkin REQUIRED COMPONENTS rviz roscpp)

find_package(catkin REQUIRED
  roscpp
)
find_package(catkin REQUIRED genmsg actionlib_msgs actionlib)
add_action_files(DIRECTORY action FILES DishWasher.action)
generate_messages(DEPENDENCIES actionlib_msgs)

catkin_package(
  CATKIN_DEPENDS
)

include_directories(${catkin_INCLUDE_DIRS})
link_directories(${catkin_LIBRARY_DIRS})

## This plugin includes Qt widgets, so we must include Qt like so:
find_package(Qt5 COMPONENTS Core Gui REQUIRED)
  set( QT_LIBRARIES Qt5::Widgets )
find_package(Qt5Widgets REQUIRED)

##include(${QT_USE_FILE})

## I prefer the Qt signals and slots to avoid defining "emit", "slots",
## etc because they can conflict with boost signals, so define QT_NO_KEYWORDS here.
add_definitions(-DQT_NO_KEYWORDS)

## Here we specify which header files need to be run through "moc",
## Qt's meta-object compiler.
QT5_WRAP_CPP(MOC_FILES
  src/dish_washer_client_gui.h
)

## Here we specify the list of source files, including the output of
## the previous command which is stored in ``${MOC_FILES}``.
set(SOURCE_FILES_GUI
  src/dish_washer_client_gui.cpp
  src/dish_washer_client_gui_node.cpp
  ${MOC_FILES}
)

## Add the "myviz" executable and specify the list of source files we
## collected above in ``${SOURCE_FILES}``.
add_executable(dish_washer_client_gui ${SOURCE_FILES_GUI})

## Link the myviz executable with whatever Qt libraries have been defined by
## the ``find_package(Qt4 ...)`` line above, and with whatever libraries
## catkin has included.
target_link_libraries(dish_washer_client_gui ${QT_LIBRARIES} ${catkin_LIBRARIES})
## END_TUTORIAL


## Here we specify the list of source files, including the output of
## the previous command which is stored in ``${MOC_FILES}``.
set(SOURCE_FILES
  src/dish_washer_server_node.cpp
)

## Add the "myviz" executable and specify the list of source files we
## collected above in ``${SOURCE_FILES}``.
add_executable(dish_washer_server ${SOURCE_FILES})

## Link the myviz executable with whatever Qt libraries have been defined by
## the ``find_package(Qt4 ...)`` line above, and with whatever libraries
## catkin has included.
target_link_libraries(dish_washer_server ${QT_LIBRARIES} ${catkin_LIBRARIES})
## END_TUTORIAL

## Install
install(TARGETS dish_washer_client_gui DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})
install(TARGETS dish_washer_server DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})