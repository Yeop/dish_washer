#include <actionlib/server/simple_action_server.h>
#include <dish_washer/DishWasherAction.h>
#include <ros/ros.h>
#include <thread>
// typedef actionlib::SimpleActionServer<action::DishWasher> Server;

#define IDEL 0
#define WASH 1
#define CLEAN 2
#define SUCCEES 3
#define FAILURE

// TODO Make Class
class DishWasherServer {
 private:
  /* data */
  ros::NodeHandle nh_;

  std::thread dish_washer_thread_;
  // dish_washer::DishWasherAction
  actionlib::SimpleActionServer<dish_washer::DishWasherAction> as_;
  // ! defining goal as a member variable.
  dish_washer::DishWasherGoal goal_;

  void ExecuteCB(const dish_washer::DishWasherGoalConstPtr &goal);

  int washer_state_;
  int washed_rack_;
  int washed_dish_;

  dish_washer::DishWasherResult result_;
  dish_washer::DishWasherFeedback feedback_;
  std::string action_name_;


 public:
  DishWasherServer(std::string name);
  ~DishWasherServer();
};

DishWasherServer::DishWasherServer(std::string name) :
    as_(nh_, name, boost::bind(&DishWasherServer::ExecuteCB, this, _1), false),
    action_name_(name)
  {
    as_.start();
  }

DishWasherServer::~DishWasherServer() {}
// void execute(const chores::DoDishesGoalConstPtr& goal, Server* as)  // Note:
// "Action" is not appended to DoDishes here
// {
//   // Do lots of awesome groundbreaking robot stuff here
//   as->setSucceeded();
// }

void DishWasherServer::ExecuteCB(
    const dish_washer::DishWasherGoalConstPtr &goal) {
  // helper variables
  ros::Rate r(5);
  bool success = true;
  double sec = 0;
  // start executing the action
  while (ros::ok()) {

    if (as_.isPreemptRequested() || !ros::ok()) {
      ROS_INFO("%s: Preempted", action_name_.c_str());
      // set the action state to preempted
      as_.setPreempted();
      success = false;
      break;
    }

    r.sleep();
    sec = sec + 0.2;

    washed_dish_ = washed_dish_ + 1;

    if (washed_dish_ >= 10) {
      washed_rack_ = washed_rack_ + 1;
      washed_dish_ = 0;
      feedback_.num_washed_rack = washed_rack_;
      
      as_.publishFeedback(feedback_);

      if (washed_rack_ >= goal->num_rack) {
        // Done!!! All of rack is washed.
        result_.second = sec; 
        result_.num_washed_rack = washed_rack_;
        result_.num_remain_rack = 0;
        success = true;
        break;
      }
    }
  } // Washing
  // Action is finished.
  if (success == true) {
    as_.setSucceeded();
  }
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "dish_washer");
  ros::NodeHandle n;

  // TODO DishWasherServer 선언.
  DishWasherServer dish_washer("dish_washer");
  ros::spin();

  return 0;
}