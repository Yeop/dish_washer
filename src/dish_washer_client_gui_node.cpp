#ifndef Q_MOC_RUN

#include <QApplication>
#include <ros/ros.h>
#include "dish_washer_client_gui.h"
DishWasherClient* myviz;
int main(int argc, char **argv)
{
  if( !ros::isInitialized() )
  {
    ros::init( argc, argv, "myviz", ros::init_options::AnonymousName );
  }

  QApplication app( argc, argv );

  DishWasherClient* myviz = new DishWasherClient();
  myviz->show();

  app.exec();
  ros::spin();
  delete myviz;
}

#endif