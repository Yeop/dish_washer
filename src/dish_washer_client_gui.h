#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <boost/thread.hpp>
#include <actionlib/client/simple_action_client.h>
#include <dish_washer/DishWasherAction.h>

#include <QWidget>
#endif


class DishWasherClient : public QWidget {
  Q_OBJECT
 public:
  DishWasherClient(QWidget* parent = 0);
  virtual ~DishWasherClient();
 
 private Q_SLOTS:
  void ClickedRunBtn();
  void ExecuteRun();
  void setNumRack(int num_rack);

 private:
  ros::NodeHandle nh_;
  int num_rack_;
  int thread_running_;

};