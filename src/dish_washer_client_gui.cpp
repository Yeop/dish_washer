#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QVBoxLayout>

#include "dish_washer_client_gui.h"
// BEGIN_TUTORIAL
// Constructor for DistancePublisherGUI.  This does most of the work of the
// class.
DishWasherClient::DishWasherClient(QWidget* parent) : QWidget(parent),num_rack_(0),thread_running_(0) {
  // Construct and lay out labels and slider controls.
  QLabel* num_rack_label = new QLabel("Num. rack");
  QSlider* num_rack_slider = new QSlider(Qt::Horizontal);
  num_rack_slider->setTickInterval(1);
  num_rack_slider->setMinimum(0);
  num_rack_slider->setMaximum(20);

  QLabel* run_label = new QLabel("Run");
  QPushButton* run_bnt = new QPushButton();
  run_bnt->setObjectName(QString::fromUtf8("Run"));

  QLabel* cleaning_label = new QLabel("Cleaning");
  QPushButton* cleaning_bnt = new QPushButton();
  cleaning_bnt->setObjectName(QString::fromUtf8("Cleaning"));

  QGridLayout* controls_layout = new QGridLayout();
  controls_layout->addWidget(num_rack_label, 0, 0);
  controls_layout->addWidget(num_rack_slider, 0, 1);
  controls_layout->addWidget(run_label, 1, 0);
  controls_layout->addWidget(run_bnt, 1, 1);
	controls_layout->addWidget(cleaning_label, 2, 0);
  controls_layout->addWidget(cleaning_bnt, 2, 1);

  // Construct and lay out render panel.
  QVBoxLayout* main_layout = new QVBoxLayout;
  main_layout->addLayout(controls_layout);

  // Set the top-level layout for this DistancePublisherGUI widget.
  setLayout(main_layout);

  // Make signal/slot connections.
  connect(num_rack_slider, SIGNAL(valueChanged(int)), this,
          SLOT(setNumRack(int)));
  connect(run_bnt, SIGNAL(clicked()), this, SLOT(ClickedRunBtn()));
	
  
  //connect(cleaning_bnt, SIGNAL(clicked()), this, SLOT( Your Function ));


  // Initialize the slider values.
  num_rack_slider->setValue(0);

  nh_ = ros::NodeHandle();
}

// Destructor.
DishWasherClient::~DishWasherClient() {}

void DishWasherClient::ClickedRunBtn(){

  // if(thread_running_){
  //   ROS_INFO("Washing...");
  //   return;
  // }else{
  //   thread_running_ = 1;
  // }
  boost::thread execute_thread(&DishWasherClient::ExecuteRun, this);
  //execute_thread.join();
  
}
void DishWasherClient::ExecuteRun() {

  actionlib::SimpleActionClient<dish_washer::DishWasherAction> ac("dish_washer");
  // TODO 서버 작업후 확인
  // ROS_INFO("Waiting for action server to start.");
  // ac.waitForServer();

  // ROS_INFO("Action server started, sending goal.");
  dish_washer::DishWasherGoal goal;
  goal.num_rack = num_rack_;

  ac.sendGoal(goal);

  //wait for the action to return
  bool is_finished_before_time = ac.waitForResult(ros::Duration((double)(num_rack_*2)+10.0));
  
  if(is_finished_before_time){
    actionlib::SimpleClientGoalState state = ac.getState();
    
    ROS_INFO("Action finished: %s",state.toString().c_str());
    if(state == actionlib::SimpleClientGoalState::SUCCEEDED){
      dish_washer::DishWasherResultConstPtr result = ac.getResult();

      ROS_INFO("second: %f",result->second);
      ROS_INFO("num_remain_rack: %d",result->num_remain_rack);
      ROS_INFO("num_washer_rack: %d",result->num_washed_rack);
    }
    // }else if(state == actionlib::SimpleClientGoalState::REJECTED){
      
    // }else if(state == actionlib::SimpleClientGoalState::RECALLED){
      
    // }else if(state == actionlib::SimpleClientGoalState::PREEMPTED){
      
    // }else if(state == actionlib::SimpleClientGoalState::ABORTED){
      
    // }
    
  }else
    ROS_INFO("Action did not finish before the time out.");

  //thread_running_ = 0;
}

void DishWasherClient::setNumRack(int num_rack) {
  num_rack_ = num_rack;
  ROS_INFO("num_rack = %d",num_rack_);
}